#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include "PrivateData.h"
#include "dht_nonblocking.h"

#define DHT_SENSOR_TYPE DHT_TYPE_11

static const int DHT_SENSOR_PIN = 12;
DHT_nonblocking dht_sensor( DHT_SENSOR_PIN, DHT_SENSOR_TYPE );

// WiFi Connection
const String SSID = WIFI_SSID;
const String PSK = WIFI_PASSWORD;
// Server SSL connection
const int SID_HUM = SENSOR_ID_HUM;
const String TOKEN_HUM = API_TOKEN_HUM;
const int SID_TEMP = SENSOR_ID_TEMP;
const String TOKEN_TEMP = API_TOKEN_TEMP;
const String HOST = "www.sensorhub.live";
const int HTTPS_PORT = 443;
// create Secure HTTPS Client
WiFiClientSecure espClient;


/*
 * Initialize the serial port.
 */
void setup( )
{
  Serial.begin( 9600);
  initWiFi();
}



/*
 * Poll for a measurement, keeping the state machine alive.  Returns
 * true if a measurement is available.
 */



/*
 * Main program loop.
 */
void loop( )
{
  float temperature;
  float humidity;

  /* Measure temperature and humidity.  If the functions returns
     true, then a measurement is available. */
  if( measure_environment( &temperature, &humidity ) == true )
  {
    Serial.print( "T = " );
    Serial.print( temperature, 1 );
    Serial.print( " deg. C, H = " );
    Serial.print( humidity, 1 );
    Serial.println( "%" );
    postHttpsData(SID_TEMP, TOKEN_TEMP, "/sensors/api/5", temperature);
    postHttpsData(SID_HUM, TOKEN_HUM, "/sensors/api/4", humidity);
    
  }
}

static bool measure_environment( float *temperature, float *humidity )
{
  static unsigned long measurement_timestamp = millis( );

  /* Measure once every four seconds. */
  if( millis( ) - measurement_timestamp > 1000*60ul )
  {
    if( dht_sensor.measure( temperature, humidity ) == true )
    {
      measurement_timestamp = millis( );
      return( true );
    }
  }

  return( false );
}


void initWiFi() {
  Serial.println("Connecting to ");
  Serial.println(SSID);
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PSK);

  //Try connecting until success
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

void postHttpsData(int id, String token, String url, int data) {

  if(WiFi.status() == WL_CONNECTED) {
    //No certification exchange => No Authentication
    espClient.setInsecure();

    //Set connection timeaout
    espClient.setTimeout(15000);
    delay(1000);
    int retry = 0;


    //Initialize payload
    StaticJsonDocument<200> payload;
    payload["body"]["id"] = String(id);
    payload["body"]["value"] = String(data);
    payload["body"]["token"] = token;
    
    //Try connection to the Server
    Serial.print("Connecting to Server ");
    while((!espClient.connect(HOST, HTTPS_PORT)) && (retry < 15)) {
      delay(100);
      Serial.print(".");
      retry++;
    }
    
    if( retry == 15 ) {
      Serial.println(" Connection failed");
    } else {
      Serial.println(" Connected to Server");
    }

    Serial.print("POST ");Serial.print(url);Serial.println(" HTTP/1.1");
    Serial.print("Host: "); Serial.println(HOST);
    Serial.println("Content-Type: application/json");
    Serial.print("Content-Length: "); Serial.println(measureJson(payload));
    Serial.println();
    serializeJson(payload, Serial);

    //Send POST Request
    espClient.print("POST");espClient.print(url);espClient.println("HTTP/1.1");
    espClient.print("Host: "); espClient.println(HOST);
    espClient.println("Content-Type: application/json");
    espClient.print("Content-Length: "); espClient.println(measureJson(payload));
    espClient.println();
    serializeJson(payload, espClient);
    
    while(espClient.connected()) {
      String line = espClient.readStringUntil('\n');
      if (line.startsWith("{\"state\":\"success\"")) {
        Serial.println("esp8266/Arduino CI successfull!");
      } else {
        Serial.println(line);
      }
    }              
  }
}
