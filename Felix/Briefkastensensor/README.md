# Briefkastensensor von Felix

Beinhaltet Arduino und ATMEL Studio 7 Projekt für den Briefkastensensor

## Idea

The Idea is that the HC-SR501 PIR sensor for the arduino detects movement inside the mailbox and sends a signal via wifi (esp8266) to a raspberry server in the LAN.
The Raspberry controls and processes the data received and sends in to a webserver for distribution.