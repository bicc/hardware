#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include "PrivateData.h"


int pirPin = 12; // Input for HC-S501
int pirValue;   // Place to store read PIR Value

// WiFi Connection
const String SSID = WIFI_SSID;
const String PSK = WIFI_PASSWORD;
// Server SSL connection
const int SID = SENSOR_ID;
const String TOKEN = API_TOKEN;
const String HOST = "www.sensorhub.live";
const int HTTPS_PORT = 443;

// create Secure HTTPS Client
WiFiClientSecure espClient;



void setup() {
  Serial.begin(9600); // chosen baud rate
  pinMode(pirPin, INPUT); 
  initWiFi(); // intitialize WiFi connection
}

void loop() {
  pirValue = digitalRead(pirPin); // 0 no Motion detected; 1 Motion detected
  Serial.print(pirValue);
  if(pirValue) {
     Serial.println("Movement detected");
     postHttpsData(pirValue); // send motion detection to sensorHub
  }

}

void initWiFi() {
  Serial.println("Connecting to ");
  Serial.println(SSID);
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PSK);

  //Try connecting until success
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}


void postHttpsData(int data) {

  if(WiFi.status() == WL_CONNECTED) {
    //No certification exchange => No Authentication
    espClient.setInsecure();

    //Set connection timeaout
    espClient.setTimeout(15000);
    delay(1000);
    int retry = 0;

    //Api path
    String url = "/api/sensors/" + String(SID);
    
    //Initialize payload
    StaticJsonDocument<200> payload;
    payload["body"]["id"] = String(SID);
    payload["body"]["value"] = String(data);
    payload["body"]["token"] = TOKEN;
    
    //Try connection to the Server
    Serial.print("Connecting to Server ");
    while((!espClient.connect(HOST, HTTPS_PORT)) && (retry < 15)) {
      delay(100);
      Serial.print(".");
      retry++;
    }
    
    if( retry == 15 ) {
      Serial.println(" Connection failed");
    } else {
      Serial.println(" Connected to Server");
    }
    
    //Print POST Request
    Serial.println();
    Serial.print("POST "); Serial.print(url); Serial.println(" HTTP/1.1");
    Serial.print("Host: "); Serial.println(HOST);
    Serial.println("Content-Type: application/json");
    Serial.print("Content-Length: "); Serial.println(measureJson(payload));
    Serial.println();
    serializeJson(payload, Serial);
    Serial.println();

    //Send POST Request
    espClient.print("POST "); espClient.print(url); espClient.println(" HTTP/1.1");
    espClient.print("Host: "); espClient.println(HOST);
    espClient.println("Content-Type: application/json");
    espClient.print("Content-Length: "); espClient.println(measureJson(payload));
    espClient.println();
    serializeJson(payload, espClient);

    //Print Response
    while(espClient.connected()) {
      String line = espClient.readStringUntil('\n');
      Serial.println(line);
    }              
  }
}
