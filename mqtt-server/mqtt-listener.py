#!/usr/bin/env python
import paho.mqtt.client as mqtt
from os import environ
from dotenv import load_dotenv
import requests
load_dotenv()

sensors = {
    "waterpump1": {
        "id": environ.get("BLUMENSENSOR_1_ID"),
        "apiToken": environ.get("BLUMENSENSOR_1_TOKEN")
    }
}


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("outTopic")


def separateMsg(payload):
    msg = str(payload, 'utf-8')
    splitted_msg = msg.split(":")
    #print(splitted_msg)
    result = {"sensorName": splitted_msg[0], "value": splitted_msg[1]}
    return result


def on_message(client, userdata, msg):
    message = separateMsg(msg.payload)
    print("message: " + str(message))
    remote_sensor = sensors[message["sensorName"]]
    print("remote sensor data: " + str(remote_sensor))

    value = int(message["value"])
    print("value: " + str(value))
    if (value > 100):  # faulty measurements
        return
    token = remote_sensor["apiToken"]
    print("token: " + str(token))
    sensor_id = remote_sensor["id"]
    print("sensor id: " + str(sensor_id))

    data = {"body": {"value": value, "token": token}}

    url = sensorshub_url + sensor_id
    print(url)
    print(data)
    result = requests.post(sensorshub_url + sensor_id, json=data)
    print(result)


if __name__ == "__main__":
    mqtt_url = environ.get('MQTT_URL')
    sensorshub_url = environ.get('SENSORHUB_URL')

    client = mqtt.Client()

    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(mqtt_url, 1883, 60)

    client.loop_forever()
