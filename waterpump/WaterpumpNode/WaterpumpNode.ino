// MQTT based on PubSubClient tutorial https://github.com/knolleary/pubsubclient/blob/master/examples/mqtt_esp8266/mqtt_esp8266.ino

#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "PrivateData.h"

SoftwareSerial s(D6,D5); // virtual serialization pins (RX,TX) to speak with arduino

// values defined as macros in PrivateData.h
const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;
const char* mqtt_server = MQTT_SERVER_IP;

WiFiClient espClient;
PubSubClient client(espClient); // mqtt client

#define MSG_BUFFER_SIZE  (50)
char msg[MSG_BUFFER_SIZE]; // message buffer for mqtt message

void setup() {
   s.begin(9600); // serial interface to esp8266 with baud rate of 9600
  Serial.begin(9600); // Serial interface for debugging with baud rate 9600
  
  setup_wifi();
  client.setServer(mqtt_server, 1883);

  Serial.println("Node setup");
}
 
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  if (s.available()>0){
    // if data received from arduino
    int data=s.read();
    Serial.print("Data from Arduino: ");
    Serial.println(data);
    snprintf (msg, MSG_BUFFER_SIZE, "waterpump1:%ld", data); // build message to send via mqtt client
    client.publish("outTopic", msg);
  } else {
    Serial.println("No data received");
  }
  delay(5000);

}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "Waterpump-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
