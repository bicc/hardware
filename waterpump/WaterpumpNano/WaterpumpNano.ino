#include <SoftwareSerial.h>

SoftwareSerial s(5, 6); // virtual serialization pins (RX,TX) to speak with esp8266

long millisecondToSecondFactor = 1000; // conversion from microseconds to seconds
long led_rhythm_baseline = (long)(0.1 * millisecondToSecondFactor); // led 10hz baseline
long led_rhythm_dynamic; // led frequency is dependent on moisture percentage to give visual feedback on the hw itself
long led_last_switch_time = 0; // next point in time to switch led status
bool led_is_active = false;

long sensor_rhythm = 1680 * millisecondToSecondFactor; // sensor measurement every ~28 minutes, 51.5 times a day (from 100 to 10) 1680
long sensor_last_measure_time = 0; // next point in time to measure
int moisture_threshold = 10; // at which percentage the pump needs to be activated

int moisture_percentage_mock = 100; // mock measurement until i have a sensor
int mock_drying_rate = 1; // this is used to decrease the sensor mock value

int relay_pin = 7; // pin to switch relay for the waterpump

void setup() {
  s.begin(9600); // serial interface to esp8266 with baud rate of 9600
  Serial.begin(9600); // serial interface to computer for debugging with baud rate 9600
  Serial.println("Nano setup");

  pinMode(relay_pin, OUTPUT); // pin to control relay is set to output
  digitalWrite(relay_pin, HIGH); // relay pin is high, therefore relay is not letting power through

  // first measurement to set initial led frequency
  int p = readMoistureSensor();
  led_rhythm_dynamic = led_rhythm_baseline * ((long)p); // baseline of 10hz times the percentage, becomes slower with high percentages

  // initial time values for event loop
  long now = millis();
  sensor_last_measure_time = now;
  led_last_switch_time = now;
}

void loop() {
  long now = millis();
  // switching led between on and off in a dynamic interval
  if (now - led_last_switch_time > led_rhythm_dynamic) {
    Serial.println("switching leds");
    if (led_is_active) {
      turnOffLed();
    } else {
      turnOnLed();
    }
    led_last_switch_time = millis();
  }

  if (now - sensor_last_measure_time > sensor_rhythm) {
    int measurement = readMoistureSensor();
    led_rhythm_dynamic = led_rhythm_baseline * ((long)measurement); // calculate led frequency for current moisture
    Serial.println("measuring moisture = " + String(measurement) + "%");
    sendMoisturePercentage(measurement); // send measurement to esp8266 via virtual serial interface
    if (measurement < moisture_threshold) {
      activateWaterpump();
    }
    Serial.println(micros());
    Serial.println(sensor_rhythm);
    sensor_last_measure_time = millis();
    Serial.println(sensor_last_measure_time);
  }
}

void turnOnLed() {
  led_is_active = true;
  digitalWrite(13, HIGH);
}

void turnOffLed() {
  led_is_active = false;
  digitalWrite(13, LOW);
}

void sendMoisturePercentage(int percentage) {
  s.write(percentage);
}

void activateWaterpump() {
  moisture_percentage_mock = 100; // mock until real sensor
  digitalWrite(relay_pin, LOW); // LOW means power can flow from the dc power supply through the relay into the pump
  delay(10000);
  digitalWrite(relay_pin, HIGH); // deactivate pump after 10 seconds
}

int readMoistureSensor() {
  // mock implementation until real sensor
  float random_factor = random(50, 300) / 100.0; // 0.5 - 3
  moisture_percentage_mock -= ( mock_drying_rate * random_factor); // decrease moisture by derivating amount
  if (moisture_percentage_mock < 0 ) { // can't be below zero
    moisture_percentage_mock = 0;
  }
  return moisture_percentage_mock;
}
