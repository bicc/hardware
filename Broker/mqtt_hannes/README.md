# Setup

## Raspberry Pi 3

* install [ARMv7](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3)
* bdstar needs [libarchive 3.3.1](https://github.com/helotism/helotism/issues/8)
* alternatively the real pi3 [os](https://linuxize.com/post/how-to-install-arch-linux-on-raspberry-pi/) (the former guide uses pi 2)
* possible further [instructions](https://www.instructables.com/id/Arch-Linux-on-Raspberry-Pi/)

## MQTT / Mosquitto

* [Guide](https://randomnerdtutorials.com/how-to-install-mosquitto-broker-on-raspberry-pi/) for usage on rasbian
* [Guide](https://dominicm.com/install-mosquitto-mqtt-broker-on-arch-linux/) with Arch
