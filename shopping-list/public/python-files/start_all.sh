#!/bin/bash
trap 'kill $(jobs -pr)' SIGINT 
python3 file_listener.py & python3 live-scanner.py &

#trap 'pkill -f file_listener.py' INT
#trap 'pkill -f live-scanner.py' SIGINT
#python3 file_listener.py &
#python3 live-scanner.py &
# sh -c 'python3 file_listener.py & python3 live-scanner.py' &
#pgid=$!
#echo "Background tasks are running in process group $pgid, kill with kill -TERM -$pgid"
#echo $(pgrep -f 'python3 file_listener.py')
#trap 'kill $(pgrep -f 'python3 file_listener.py')' SIGINT
