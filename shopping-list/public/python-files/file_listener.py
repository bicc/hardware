import hashlib
import time
from git import Repo

# On Pi:
# PATH
# PATH_OF_GIT_REPO = r'json/.git'  
PATH_OF_GIT_REPO = r'/home/pi/Desktop/shopping-list/public/json/.git'  # make sure .git folder is properly configured
COMMIT_MESSAGE = 'auto commit'
PATH_TO_JSON = '../json/data.json'


def git_push():
    try:
        print("hello")
        repo = Repo(PATH_OF_GIT_REPO)
        print("There")
        repo.git.add(update=True)
        repo.index.commit(COMMIT_MESSAGE)
        origin = repo.remote(name='origin')
        origin.push()
        print("Push to git successful")
    except:
        print('Some error occured while pushing the code')


def get_hash_from_json():
    hasher = hashlib.md5()
    with open(PATH_TO_JSON, 'rb') as f:
        buf = f.read()
        hasher.update(buf)
        file_hash = hasher.hexdigest()
        print(file_hash)
        return file_hash


def main():
    origin_file = get_hash_from_json()
    while 1:
        time.sleep(10)
        changed_file = get_hash_from_json()
        print(origin_file == changed_file)
        if origin_file != changed_file:
            git_push()
            origin_file = changed_file


if __name__ == '__main__':
    main()
