import cv2
from pyzbar import pyzbar
import imutils
from imutils.video import VideoStream
import time
from barcode_from_picture import check_food

# initialize video stream at FullHD with Raspi cam
vs = VideoStream(usePiCamera=True, resolution=(1920, 1440)).start()
# time.sleep(2.0)
count = 0

while True:
    frame = vs.read()
    # frame = imutils.resize(frame, width=400) // if faster handle needed
    # cv2.imwrite("frames/frame%d.jpg" % count, frame)
    count += 1
    barcodes = pyzbar.decode(frame)
    print(barcodes)
    for barcode in barcodes:
        print(barcode.type)
        check_food(barcode.data.decode('utf-8'))
        from pygame import mixer
        mixer.init()  # you must initialize the mixer
        alert = mixer.Sound('scanner_beep.wav')
        alert.play()
