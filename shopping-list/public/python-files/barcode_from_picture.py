from PIL import Image
from pyzbar.pyzbar import decode
import glob
import os
import requests
import json

ean_codes = []
# On Pi:
root_dir = "/home/pi/Desktop/shopping-list/public/"
# On Mac:
# root_dir = "/Users/daniel/Desktop/HFT/UC/webserver/server/public/"
json_dir = root_dir + "json/"
upload_dir = root_dir + "uploads"


def write_json(data, filename=json_dir + 'data.json'):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)


def add_to_json(name):
    with open(json_dir + 'data.json') as json_file:
        data = json.load(json_file)
        for item in data['items']:
            if name == item['name']:
                print("Item \"" + name + "\" already on the list \n-- skipped")
                return
        data['items'].append({
            'name': name
        })
        print("Item \"" + name + "\" added to the list")
    write_json(data)


def get_food_from_barcode(barcode):
    response = requests.get(
        "https://de.openfoodfacts.org/api/v0/product/" + barcode + ".json")
    # print(response.status_code)
    if response.status_code == 200:
        # print(response.json())
        food_json = response.json()
        if food_json['status'] == 1:
            try:
                name = food_json['product']['product_name_de']
                name2 = food_json['product']['generic_name_de']
            except KeyError:
                # print("No german product found, searching globally")
                try:
                    name2 = food_json['product']['product_name_en']
                    name = food_json['product']['product_name']
                except KeyError:
                    return 0, 0
            return name, name2
        else:
            return 0, 0
    return 0, 0


def check_food(barcode):
    print("Barcode: " + barcode)
    food_name, food_name2 = get_food_from_barcode(barcode)
    if food_name == 0 and food_name2 == 0:
        print("No item found for barcode: " + barcode)
    else:
        if not food_name2:
            print("Product: " + food_name)
            add_to_json(food_name)
        elif not food_name:
            print("Article: " + food_name2)
            add_to_json(food_name2)
        else:
            print("Product: " + food_name + "\nArticle: " + food_name2)
            add_to_json(food_name)


def decode_file(file):
    # print(file)
    data = decode(Image.open(file))
    for barcode in data:
        # do anything with that data
        # print('Type of Code : ', barcode.type)
        # print('Numbers : ', str(barcode.data), '\n')
        ean_code = barcode.data.decode('utf-8')
        ean_codes.append(ean_code)


def decode_files():
    # upload_dir_relative_name = "./uploads"
    for filename in os.listdir(upload_dir):
        if filename.endswith(".jpg") or filename.endswith(".png") or filename.endswith(".jpeg"):
            decode_file(upload_dir + "/" + filename)
        # print(os.path.dirname(filename))


# after all: delete picture
def delete():
    test = os.listdir(upload_dir)
    for item in test:
        if item.endswith(".jpg") or item.endswith(".png") or item.endswith(".jpeg"):
            # print("Delete:" + os.path.join(json_dir, item))
            os.remove(os.path.join(upload_dir, item))


def main():
    decode_files()
    if not ean_codes:
        print("No Barcodes found!")
    for code in ean_codes:
        check_food(code)
        print("")
    delete()

main()
