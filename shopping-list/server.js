const express = require('express');
const formidable = require('formidable');
const { spawn } = require('child_process');
const fs = require('fs');

var app = express();

app.use(express.json());       // to support JSON-encoded bodies

app.use(express.static('public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/upload', function (req, res) {
    res.sendFile(__dirname + '/upload.html')
});

/**
 * calls python files from node
 */
app.get('/python', function (req, res) {
    var dataToSend;
    // spawn new child process to call the python script
    const python = spawn('python3', ['public/python-files/barcode_from_picture.py']);
    console.log(python);
    dataToSend = "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head><body><textarea cols=\"40\" rows=\"10\" style=\"font-size: 12px; font-family: 'Courier New', Courier, monospace;\">"
    // collect data from script
    python.stdout.on('data', function (data) {
        console.log('Pipe data from python script ...');
        dataToSend += data.toString() + "";
    });

    python.on('close', (code) => {
        console.log(`child process close all stdio with code ${code}`);
        dataToSend += "</textarea><br /><a href=\"/\" onclick=\"javascript:history.go(-1)\" style=\"font-family: 'Courier New', Courier, monospace;\">Go Back</a></body></html>"
        res.send(dataToSend)
    });
})

/**
 * picture upload request
 */
app.post('/upload', function (req, res) {
    var form = new formidable.IncomingForm();

    form.parse(req);

    form.on('fileBegin', function (name, file) {
        file.path = __dirname + '/public/uploads/' + file.name;
    });

    form.on('file', function (name, file) {
        console.log('Uploaded ' + file.name);
        res.redirect('/python')
    });
});

/**
 * current shopping list as json
 */
app.get('/json', function (req, res) {
    fs.readFile('public/json/data.json', 'utf8', (err, data) => {
        if (err) {
            throw err;
        }
        res.send(JSON.parse(data));
    });
});

/**
 * shopping cart of items
 */
app.get('/selection', function (req, res) {
    res.sendFile(__dirname + '/selection.html')
});

/**
 * parse json callback and write to data.json
 */
app.post("/selection", (req, res) => {
    // var someField1 = req.body.some_form_field;
    fs.readFile('public/json/data.json', 'utf8', function readFileCallback(err, data) {
        if (err) {
            console.log(err);
        } else {
            data = JSON.parse(data);
            req.body.forEach(element => {
                data.items.push(element);
            });
            console.log(req.body);

            json = JSON.stringify(data);
            fs.writeFile('public/json/data.json', json, 'utf8', function (err) {
                if (err) throw err;
            });
        }
    });
});


app.post("/", (req, res) => {
    // var someField1 = req.body.some_form_field;
    fs.readFile('public/json/data.json', 'utf8', function readFileCallback(err, data) {
        if (err) {
            console.log(err);
        } else {
            var data = {};

            var newData = { items: [] }
            //let newJson = JSON.parse(newData)
            req.body.forEach(element => {
                newData.items.push(element);
            });



            //console.log(req.body);
            console.log(newData);
            json = JSON.stringify(newData);
            fs.writeFile('public/json/data.json', json, 'utf8', function (err) {
                if (err) throw err;
            });
        }
    });
});

app.listen(5000);
